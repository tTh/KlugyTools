# DumpGDBM

GNU dbm (ou GDBM, en abrégé) est une
bibliothèque de fonctions de base de données qui utilise un hachage extensible,
une association `key/value` persistante.

https://en.wikipedia.org/wiki/DBM_(computing)

## Exemple

Le mini-script Perl `exemple.pl` montre bien comment faire une "base de données
associative" en Perl. Il extrait les champs _usernale_ et _GECOS_ du fichier
`/etc/passwd` et les injecte dans un fichier gdbm.

```
tth@redlady:~/Devel/KlugyTools/DumpGDBM$ ./exemple.pl 
tth@redlady:~/Devel/KlugyTools/DumpGDBM$ ./dumpgdbm -i exemple.gdbm 
working on [exemple.gdbm]

dumpgdbm > first
K: rtkit.
D: RealtimeKit,,,.

dumpgdbm > next
K: man.
D: man.

dumpgdbm > next
K: lightdm.
D: Light Display Manager.

dumpgdbm > ? ks

command [ks]
  Search a posix regex pattern in all the key fields.

dumpgdbm > ks root
K: root.
D: root.

dumpgdbm > ks tth
K: tth.
D: tTh,,,.

dumpgdbm > 
EOF ?
tth@redlady:~/Devel/KlugyTools/DumpGDBM$ 
```

Well done bro.


