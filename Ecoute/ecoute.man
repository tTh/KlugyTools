.TH Ecoute 1 "2005 September" "various OS" "Tonton Th"

.SH NAME
ecoute \- ncurses based note,wav,ogg,au player.

.SH SYNOPSYS
\fBecoute\fP 

.SH OPTIONS
-s <newdir> : change working directory to <newdir>.

.SH INTERACTIVE
This player is a real ncurses based interactive software.
Full of bugs, a lot of to_be_done functions. You can try
the '?' key at main screen.

.SH CONFIG FILE
All your config files are belong to us. 

.SH WHAT IS A .note FILE ?
This a raw, monophonic, 16 bits, 24000 smpl/s binary file.
You can seek the web for a 'ziktth' software if you need more informations.


.SH AUTHOR(S)
Thierry (aka tth) Boudet. http://tboudet.free.fr/

.SH DEDICACE
This software is dedicated to DEC, who build the pdp11.

