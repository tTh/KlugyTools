/*
 *		------
 *		Ecoute
 *		------  an ogg/note/wav player from tth
 *
 * 15 juillet 2007: ogg palying don(t work, sorry...
 *
 */

#include    <stdlib.h>
#include    <unistd.h>
#include    <string.h>
#include    <getopt.h>

#include    "ecoute.h"

/*==------------------------------------------------------------------==*/
static char *about_texte[] = 
	{
	"{ About... }",
	"Ecoute, a sound player - version " VERSION,
	"Another ugly software made by tontonTh",
	"",
	"Send bugs reports: tontonth(O)free(o)fr",
	"",
	"Binary build: " __DATE__ "  /  " __TIME__,
	NULL
	};
static char *help_texte[] = 
	{
	"{ Help me }",
	"<enter>     Play the selected file",
	"A           About this software...",
	"D           Dump begin of file in hexadecimal",
	"R           Play a randomly selected file",
	"nN          Sort by name",
	"sS          Sort by file size",
	"tT          Sort by file time",
	"uU          Sort by order (unsort)",
	"I           Infos about this file",
	"$           Technical infos",
	"",
	"Q           Quit",
	NULL
	};
void popup_texte(char *lignes[])
{
WINDOW * popup;
int	nblignes;
int	foo, largmax;
char	**ptr;

nblignes = largmax = 0;
ptr = lignes+1;
while ( *ptr != NULL ) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%p  %s\n", ptr, *ptr);
#endif
	foo = strlen(*ptr);
	if (largmax < foo)	largmax = foo;
	nblignes++;
	ptr++;
	}

popup = newwin((nblignes)+4, largmax+7, L_POPUP, C_POPUP);
bordure(popup, 1);
wrefresh(popup);

for (foo=1; foo<=nblignes; foo++) {
	mvwaddstr(popup, (foo)+1, 3, lignes[foo]);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%4d %s\n", foo, lignes[foo]);
#endif
	}
mvwaddstr(popup, 0, 5, *lignes);		/* window title */
wmove(popup, 0, 0);
wrefresh(popup);
getch();
delwin(popup);	
			/* pourquoi, quand je fait le 'delwin', ncurses
			   ne rafraichit pas la zone qui �tait masqu�e ? */
touchwin(stdscr); refresh();
}
/*==------------------------------------------------------------------==*/
void about(void)
{
#if DEBUG_LEVEL > 1
fprintf(stderr, "------------ about\n");
#endif
popup_texte(about_texte);
}
void help(void)
{
#if DEBUG_LEVEL > 1
fprintf(stderr, "------------ help\n");
#endif
popup_texte(help_texte);
}
/*==------------------------------------------------------------------==*/
void help_cli(char *command)
{
printf("Usage: %s <options>\n", command);
puts("Options:");
puts("\t-d\taudio device");
puts("\t-h\tthis help");
puts("\t-x\tenable crash");

exit(0);
}
/*==------------------------------------------------------------------==*/
/*									*/
int main(int argc, char * argv[])
{
int		opt = 0;
int		foo;
char		*audiodevice = "none";
char		*newdir = NULL;

#if DEBUG_LEVEL
fprintf(stderr, "\n\tE C O U T E   %d\n\n", getpid());
#endif

while ((opt = getopt(argc, argv, "d:hs:x")) != -1) {
	switch (opt) {
		case 'd':
			audiodevice = optarg;
			break;
		case 'h':
			help_cli(argv[0]);		break;
		case 's':
			newdir = optarg;
			break;
		case 'x':
			fprintf(stderr, "no crash available\n");
			exit(0);
		default:
			fprintf(stderr, "gni %c ?\n", opt);
			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "audio device = %s\n", audiodevice);
#endif

if (NULL != newdir) {
#if DEBUG_LEVEL
	fprintf(stderr, "newdir = '%s'\n", newdir);
#endif
	foo = chdir(newdir);
	}

foo = init_sound_output(audiodevice, 0);
if (foo) {
	fprintf(stderr, "init sound output -> %d\n", foo);
	exit(1);
	}

#if DEBUG_LEVEL
infos_sound_output("dans main");
#endif

prepare_ecran();

fileselector();

say_goodbye();

return 0;
}
/*==------------------------------------------------------------------==*/
