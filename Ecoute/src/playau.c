/*
 *	playau.c
 *	--------			2 Mars 2005
 *
 *	This code is almost a copy'n'past from "playwav.c", but
 *	it was slightly updated. May be I have introduced new bugs,
 *	who have to be backported :)
 *
 */

#include   <fcntl.h>
#include   <string.h>
#include   <sndfile.h>
#include   <ao/ao.h>		/* for the sound output */

#include   "ecoute.h"

/*==------------------------------------------------------------------==*/
int	au_player(char *fname, WINDOW *popup)
{
SNDFILE			* sndf;
SF_INFO			sfinfo;
ao_sample_format	format;
ao_device		* device;
int			driver;

char			chaine[100];
short			* samples;
long			total_lu;
int			foo, bar, lu;
int			maxsample;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, fname, popup);
#endif

/*
 *	get help from 'sndfile' for getting datas
 */
memset(&sfinfo, 0, sizeof(sfinfo));

sndf = sf_open(fname, SFM_READ, &sfinfo);
if (NULL == sndf)	{
	mvwaddstr(popup, 4, 4, "Invalid file ?");
	wrefresh(popup);
	getch();
	delwin(popup);
	touchwin(stdscr);	refresh();
	return -1;
	}

sprintf(chaine, "frames         %lu", (unsigned long)sfinfo.frames);
mvwaddstr(popup, 2, 5, chaine);
sprintf(chaine, "sample rate    %d", sfinfo.samplerate);
mvwaddstr(popup, 3, 5, chaine);
sprintf(chaine, "channels       %d", sfinfo.channels);
mvwaddstr(popup, 4, 5, chaine);
sprintf(chaine, "format         %x", sfinfo.format);
mvwaddstr(popup, 5, 5, chaine);
wrefresh(popup);

/*
 *	memory allocation
 *	warning, we can get multiples channels.
 */
if ( NULL == (samples = malloc(T_BUFFER*sizeof(short)))) {
	perror("no memory in AU player\n");
	abort();
	}

/*
 *	configuration of the sound output
 */
ao_initialize();
#if DEBUG_LEVEL
fprintf(stderr, "%s: ao ititialised\n", __func__);
#endif
	
driver = ao_default_driver_id();
#if DEBUG_LEVEL
fprintf(stderr, "%s: ao default driver = %d\n", __func__, driver);
#endif

memset(&format, 0, sizeof(format));
format.bits		= 16;				/* ? */
format.channels		= sfinfo.channels;
format.rate		= sfinfo.samplerate;
format.byte_format	= AO_FMT_LITTLE;		/* XXX ??? */

fprintf(stderr, "%s: going to open ao\n", __func__);

device = ao_open_live(driver, &format, NULL);
fprintf(stderr, "%s: open live -> %p\n", __func__, device);
if (NULL == device) {
	fprintf(stderr, "\nEcoute: Error open device\n");
	/*
	 *  comment connaitre la cause de l'erreur ?
	 */
	abort();  
	}

/*
 *	Go! Read the samples, compute some numbers and...
 *	...???
 */
total_lu = 0L;
maxsample = 0;

#if DEBUG_LEVEL
fprintf(stderr, "%s: enter loop\n", __func__);
#endif
while ( (lu=sf_read_short(sndf, samples, T_BUFFER)) > 0 ) {
	ao_play(device, (char *)samples, lu*2);
	for (foo=0; foo<lu; foo++) {
		bar = abs(samples[foo]);
		if (bar > maxsample) {
			sprintf(chaine, "%9ld   max %6d", total_lu, bar);
			mvwaddstr(popup, 7, 1, chaine);
			wrefresh(popup);
			maxsample = bar;
			}
		}
	maxsample = 0;

	total_lu += (long)lu;
	progress_bar(popup, total_lu/sfinfo.channels, sfinfo.frames, 1);
	wrefresh(popup);
	}

#if DEBUG_LEVEL
fprintf(stderr, "maxsample = %d\n", maxsample);
#endif

/*
 *	some cleanup...
 */
free(samples);					/* kill buff */
sf_close(sndf);
ao_close(device);	ao_shutdown();	

return -1;
}
/*==------------------------------------------------------------------==*/
