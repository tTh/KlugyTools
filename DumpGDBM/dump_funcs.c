/*
		various functions for dumpgdbm
		==============================

    --------------------------------------------------------------------
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    --------------------------------------------------------------------

			(g) 2003 Thierry Boudet - aka TontonTh

*/

#include  <stdio.h>
#include  <gdbm.h>
#include  <ctype.h>

#include  <regex.h>

#include  "dumpgdbm.h"

/*  ---------------------------------------------------------------------  */
void
print_version(int totale)
{
printf ("\n");
printf ("*** DumpGDBM version %s (g) 2003 TontonTh Useless Soft.\n", VERSION);
printf ("\n");
if (totale)
	{
	printf (" more info -> http://krabulator.free.fr/devel/\n");
	printf (" this software is released under the 'Gnu Public License'\n");
	printf (" %s\n", gdbm_version);
	printf ("\n");
	}
#if TRACE
printf (" WARNING: compiled with TRACE option. May be _bogus_ !\n\n");
#endif
}
/*  ---------------------------------------------------------------------  */
void
print_options(void)
{
printf ("command-line options:\n");
printf ("\t-V\tprint version and infos.\n");
printf ("\t-x\thexadecimal display.\n");
printf ("\t-o\toctal display.\n");
printf ("\t-a\tascii display.\n");
printf ("\t-v\tbe verbose. more tchatche.\n");
printf ("\t-w\twarn if key empty. [todo]\n");
printf ("\t-W\tsilent search empty key. [todo]\n");
printf ("\t-m 42\tset the qty of bytes displayed.\n");
printf ("\t-8\tdisplay chars with ascii>127\n");
printf ("\t-i\tstart in interactive mode\n");
printf ("\n");
}
/*  ---------------------------------------------------------------------  */
void init_global_vars(void)
{
verbosity = 0;
maxoctets = 75;
hexadecimal = 0;
octal = 0;
ok_for_8_bits = 0;
}
/*  ---------------------------------------------------------------------  */
int
hexadump(void *ptr, int count)
{
int	i, j;
unsigned char *cptr;

cptr = (unsigned char *)ptr;
if (count > maxoctets)	count = maxoctets;

j = 0;

for (i=0; i<count; i++)
	{
	printf(" %02x", cptr[i]);
	if (j++ > 22 && i!=(count-1))
		{
		printf("\n   ");
		j = 0;
		}
	}
printf("\n");
return 0;
}
/*  ---------------------------------------------------------------------  */
int
octaldump(void *ptr, int count)
{
int	i, j;
unsigned char *cptr;

cptr = (unsigned char *)ptr;
if (count > maxoctets)	count = maxoctets;

j = 0;

for (i=0; i<count; i++)
	{
	printf(" %03o", cptr[i]);
	if (j++ > 16 && i!=(count-1))
		{
		printf("\n   ");
		j = 0;
		}
	}
printf("\n");
return 0;
}
/*  ---------------------------------------------------------------------  */
int
asciidump(void *ptr, int count)
{
int	i, j;
unsigned char *cptr;

cptr = (unsigned char *)ptr;
if (count > maxoctets)	count = maxoctets;

#if TRACE
fprintf(stderr, "ok_for_8_bits %d\n", ok_for_8_bits);
#endif

j = 0;
for (i=0; i<count; i++)
	{
	if ( ok_for_8_bits )
		{
		if (isprint(cptr[i]) || cptr[i]>127)
			printf("%c", cptr[i]);
		else
			printf(".");	
		}
	else
		{
		if (isprint(cptr[i]))
			printf("%c", cptr[i]);
		else
			printf(".");	
		}
	if (j++ > 70 && i!=(count-1))
		{
		printf("\n   ");
		j = 0;
		}
	}
printf("\n");
return 0;
}
/*  ---------------------------------------------------------------------  */
