#ifdef MAIN
	#define EXTERN
#else
	#define EXTERN extern
#endif

#define	VERSION	"0.2.27" 
 
/*  ---------------------------------------------------------------------  */
/*			global variables				   */

EXTERN int	verbosity;
EXTERN int	maxoctets;
EXTERN int	hexadecimal;
EXTERN int	octal;
EXTERN int	ok_for_8_bits;

/*  ---------------------------------------------------------------------  */

void print_version(int);
void print_options(void);
void init_global_vars(void);

int hexadump(void *ptr, int count);
int octaldump(void *ptr, int count);
int asciidump(void *ptr, int count);
