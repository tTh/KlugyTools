# Écoute, un player...

Écoute n'est pas un player comme les autres.
Vous êtes prévenus.
Certaines fonctionalités que l'on nomme *classiques*, comme
le support des *playlists* ou la recherche de pochette
de disque ne sont pas là.

En fait, j'ai commencé à écrire ce logiciel il y a très
longtemps pour découvrir
[libsndfile](https://en.wikipedia.org/wiki/Libsndfile).
Ensuite, je l'ai un peu oublié dans son coin.
Il manque même pas mal de formats de base (ex, le .ogg) dans
ce qu'il connait. Mais je
viens de lui trouver une nouvelle utilité, il va m'aider
à trier les fichiers de mon [Tascam](https://www.thomann.de/intl/tascam_dr_05x.htm). Il faut juste rajouter les fonctions
qui manquent.


## Compilation

Il faut installer les paquets
`libao-dev`, `libogg-dev`,  et `libsndfile-dev` avoir de
pouvoir générer le moindre exécutable.
Les machins *ncurses* sont censé être là, sinon
c'est `ncurses-dev` qui manque.

Ensuite, un tout simple
run de `make` fera le travail.
Dans le [Makefile](Makefile), vous avez quelques options à
régler, genre le `DEBUG_LEVEL` si vous ne voulez pas
submerger votre stderr.
Mais si vous avez activé cette friture, vous
lancer le logiciel par `./ecoute 2> tracelog` dans un xterm,
puis vous lancez `tail -f tracelog` dans un autre xterm
pour le voir raconter sa vie. *Astuce !*

## Utilisation

Une fois lancé, le logiciel vous affiche la liste des
fichiers sons dans le répertoire courant. Parfois il
en manque.

La touche **`?`** affiche une petite fenêtre d'aide.
La touche **`Q`** (*:q!*) sort de ce machin.

Il y a des fonctions de tri (nom, taille, ...) par
les touches dédiées (voir l'aide).
En pressant **`I`** des informations diverses et 
souvent inutiles sur le fichier pointé.
Le **`D`** propose un dump hexadécimal et ascii qui
va être grandement amélioré dans les jours qui viennent.
Et enfin avec **`$`**, vous aurez quelques informations techniques
sur les trucs techniques.


## Pour la suite ?

Première étape : Prévoir la possibilité de faire un *abort*
pendant la lecture d'un fichier son. Ça ne va pas être simple, il
faut d'abord factoriser la fonction de scrutation du clavier
pendant la lecture du son, et ensuite la brancher dans les
différents modules.

Deuxième étape : Implémenter une fonction bien *molly-guarded* pour pouvoir
effacer un fichier sans le moindre risque d'erreur.

Troisième étape : Ajouter un controle du volume sonore, ce qui implique
de replonger dans `alsa` ou la doc de la libao...

tTh.






