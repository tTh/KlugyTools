/*
 *	playnote.c
 *	----------			23 Fevrier 2005
 * 
 *  this subplayer is a personnal subplayer. Far back in the past,
 *  I've made a lot of softwares around sound synteses. The unique
 *  file format was 16bits, 2400samples/seconds and Intel endianism.
 *
 *		Here is the player.
 */

#include   <unistd.h>
#include   <fcntl.h>
#include   <ao/ao.h>		/* for the sound output */
#include   "ecoute.h"

/*==------------------------------------------------------------------==*/
int note_player(char *fname, WINDOW *popup)
{
int			driver;
ao_sample_format	format;
ao_device		*device;
int			lu, fd;
short			*samples;
char			chaine[100];
long			taille, total;

/*
 *	file and memory stuff
 */
if ( NULL == (samples = malloc(T_BUFFER*sizeof(short))) )
	{
	perror("memory in note_player");
	abort();
	}
if ( 0 > (fd=open(fname, O_RDONLY)) )
	{
	perror(fname);
	abort();
	}
taille = taille_fichier(fd);
if (taille < 0) {
	fprintf(stderr, "%s: size < 0        WTF?\n", __func__);
	}
sprintf(chaine, "%10ld samples, %5ld seconds", taille/2, taille/(24000*2));
mvwaddstr(popup, 2, 2, chaine);
wrefresh(popup);

ao_initialize();			/* must be paired with ao_shutdown */
driver = ao_default_driver_id();

format.bits		= 16;
format.channels		= 1;
format.rate		= 24000;
format.byte_format	= AO_FMT_LITTLE;		/* XXX ??? */
device = ao_open_live(driver, &format, NULL);
if (device == NULL)
	{
	fprintf(stderr, "\r\n%s: Error open device\n", __func__);
	/* comment connaitre la cause de l'erreur ? */
	abort();
	}

/*
 *		+-------------------------------+
 *		|   let's the show go on...     |
 *		+-------------------------------+
 */
total = 0L;
while ( (lu=read(fd, samples, T_BUFFER)) > 0 )
	{
#if DEBUG_LEVEL
	sprintf(chaine, "%9ld Kb    %-7d", total/1024, lu);
#else
	sprintf(chaine, "%9ld Kb", total/1024);
#endif
	mvwaddstr(popup, 5, 7, chaine); wrefresh(popup); 
	total += lu;
	progress_bar(popup, total, taille, 0);
	ao_play(device, (char *)samples, lu);
	}

/*
 *	this is the end, my friends...
 */
free(samples);
ao_close(device);
ao_shutdown();				/* must be paired with ao_initialise */

return -1;
}
/*==------------------------------------------------------------------==*/
