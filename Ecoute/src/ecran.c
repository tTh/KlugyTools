/*
 *	ecran.c
 *	-------
 * 
 *  Maybe, one day, we can have colors...
 *
 */

#define _POSIX_C_SOURCE 600L			/* for fileno(3) */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  <sys/select.h>

#include  "ecoute.h"

/*==------------------------------------------------------------------==*/
/*
 *	global variables
 */
int	couleur = 0;
/*==------------------------------------------------------------------==*/
void bordure(WINDOW * w, int flag)
{
if (flag)	box(w, 0, 0);
else		wborder(w, '|', '|', '-', '-', '+', '+', '+', '+');
}
/*==------------------------------------------------------------------==*/
int alerte(char *texte, int flag)
{
/*
 * this func is a stub. real implementation for the next release.
 */
return -1;
}
/*==------------------------------------------------------------------==*/
#define T_MAX_PB 90
int	progress_bar(WINDOW *win, long val, long maxval, int type)
{
int	foo, largeur;
int	larg_win, haut_win;
int	curseur;
char	chaine[T_MAX_PB+1];

/*
 *	premier defi, retrouver largeur et hauteur de la fenetre
 */
larg_win = getmaxx(win);
haut_win = getmaxy(win);

largeur = (T_MAX_PB<larg_win ? T_MAX_PB : larg_win) - 6;

#if DEBUG_LEVEL > 1
sprintf(chaine, "> x %-4d y %-4d l %-4d <", larg_win, haut_win, largeur);
mvwaddstr(win, 1, 1, chaine);
wrefresh(win);
#endif

curseur = (val*largeur) / maxval;

#if DEBUG_LEVEL > 1
fprintf(stderr, "maxval %ld  val %ld  largeur %d  curseur %d\n",
			maxval, val, largeur, curseur);
#endif

chaine[0] = '[', chaine[largeur] = ']', chaine[largeur+1] = '\0';
for (foo=1; foo<largeur; foo++) {
	if (type) {
		if (foo<curseur)	chaine[foo] = 'o';
		else			chaine[foo] = '-';
		}
	else	{
		if (foo<curseur)	chaine[foo] = '*';
		else			chaine[foo] = '.';
		}

	}
mvwaddstr(win, haut_win-3, 2, chaine);
wrefresh(win);

foo = 42;
return foo;
}
/*==------------------------------------------------------------------==*/
/*
 *  detection de la frappe d'une touche clavier -- remember kbhit() ?
 */
int is_a_key_hitted(void)
{
int		fd,retv;
fd_set		in_fds;
struct timeval	tv;

fd = fileno(stdin);
#if DEBUG_LEVEL
fprintf(stderr, "%s: fileno -> %d\n", __func__, fd);
#endif

FD_ZERO(&in_fds);	FD_SET(fd, &in_fds);
tv.tv_sec = tv.tv_usec = 0;

retv = select(1, &in_fds, NULL, NULL, &tv);
#if DEBUG_LEVEL
fprintf(stderr, "%s: select -> %d\n", __func__, retv);
#endif

if (-1 == retv) {
	perror("select()");
	}
else if (0 == retv) {
	/* no key hit */
	}
else	{
	/* got a keypress */
	return 1;
	}

return 0;
}
/*==------------------------------------------------------------------==*/
void termine_ecran(void)
{
endwin();	exit(0);
}
/*==------------------------------------------------------------------==*/
void prepare_ecran(void)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( )\n", __func__);
#endif

initscr();

if (has_colors()) {
	use_default_colors();
	couleur = 1;
	}

nonl();		cbreak();	noecho();

keypad(stdscr, TRUE);		/* acces aux touches 'curseur' */

bordure(stdscr, 1);
standout();
mvaddstr(0, 5, "{ Ecoute v " VERSION " - by tTh }");
mvaddstr(LINES-1, 5, "{ hit '?' for help, 'Q' for quit }");
standend();
refresh();

atexit(termine_ecran);
}
/*==------------------------------------------------------------------==*/
void	say_goodbye(void)
{
bordure(stdscr, 0);
wrefresh(stdscr);
}
/*==------------------------------------------------------------------==*/
