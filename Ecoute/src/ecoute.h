/*
 *	ecoute.h
 *	--------		Fevrier 2005 a Frontignan-Plage
 *
 */
#include  <ncurses.h>

#define		T_BUFFER	8192

/*==------------------------------------------------------------------==*/
void	about(void);
void	help(void);
/*==------------------------------------------------------------------==*/
/*
 *	ecran.c - ncurses related functions and global vars.
 */
#define		L_POPUP		5
#define		C_POPUP		9

void	bordure(WINDOW * w, int flag);
int	progress_bar(WINDOW *win, long val, long maxval, int type);
void	prepare_ecran(void);
void	say_goodbye(void);

extern int	couleur;

/*==------------------------------------------------------------------==*/
/*
 *	interactive.c - choice a file to play
 */
#define SORT_NAME_A	1
#define SORT_NAME_B	2

int	fileselector(void);

/*==------------------------------------------------------------------==*/
/*
 *	sound file types
 */
#define		SON_UNKNOW	0
#define		SON_NOTE	42
#define		SON_WAV		95
#define		SON_OGG		20
#define		SON_SPEEX	21
#define		SON_FLAC	22
#define		SON_OPUS	23
#define		SON_MP3		24
#define		SON_AU		128

/*
 *	players functions
 */
int	note_player(char *fname, WINDOW *popup);
int	wav_player(char *fname, WINDOW *popup);
int	au_player(char *fname, WINDOW *popup);
int	ogg_player(char *fname, WINDOW *popup);
int	speex_player(char *fname, WINDOW *popup);
int	flac_player(char *fname, WINDOW *popup);
/*==------------------------------------------------------------------==*/
/*
 *	_magic.c_ 	check if the file is a sound file.
 */
int	magic_detect(char *filename, long *pb);
void	magic_plop(int type);

/*==------------------------------------------------------------------==*/
/*
 *	'fonctions.c'
 */
long	taille_fichier(int fd);
int	type_du_fichier(char *nom);
char	*type2ext(int type);
char	*type2descr(int type);

/*
 *	Flags  is a bitfield
 */
int	play_this_file(char *nom, int type, int flags);

/*
 *	new, 18 Avril 2005, proto may change.
 */
int	info_about_this_file(char *nom, int type);

/*==------------------------------------------------------------------==*/
/*
 *	hexadecimal dumper.
 */
int	dump_this_file(char *fname, int flag);

/*
 *	libao technical interface
 */
int	init_sound_output(char *dev, int k);
int	setup_sound_output(int splr, int nbch);
int	start_sound_output(int k);
int	stop_sound_output(int k);
int	infos_sound_output(char *title);

void  infos_techniques(void);

/*==------------------------------------------------------------------==*/
