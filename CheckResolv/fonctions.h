/*
 *		CheckResolv
 *		===========
 *	quick and dirty debugging hack.
 *
 */

int	is_IP_addr(char *str);
double	chronometre(void);

int	print_ip(struct in_addr **liste);

void	help_options(void);
void	usage(void);
void	version(void);

int	print_envvars(int flag);
