# KlugyTools

Divers outils pour faire des choses diverses...

Beaucoup de choses anciennes que je traine de systèmes en systèmes, et qui ont parfois une utilité limitée.
Du code qui a parfois presque vingt ans, et qui
nécessite donc un peu de nettoyage.

## DumpGDBM

Pour le [debug](DumpGDBM) des fichiers `key/data` géres par GDBM.
Ligne de commande avec une interface _readline_.

```
tth@redlady:~/Devel/KlugyTools/DumpGDBM$ ./dumpgdbm -i exemple.gdbm 
working on [exemple.gdbm]
dumpgdbm > ?
----------------( Help me Obi Wan ! )---------------
q ZZ :q :q! exit quit bye x hex hexa a ascii o octal h help ? f first 
m maxoctets maxbytes n next p print ks keysearch ds datasearch empty 7 
8 V version commands listcom sux 
-----------( use '? command' for details )----------
dumpgdbm > p
K: rtkit.
D: RealtimeKit,,,.

dumpgdbm > n
K: lightdm.
D: Light Display Manager.

dumpgdbm > hex
dumpgdbm > p
K:  6c 69 67 68 74 64 6d 00
D:  4c 69 67 68 74 20 44 69 73 70 6c 61 79 20 4d 61 6e 61 67 65 72 00
```

## Hexdiff

Pour comparer visuellement deux fichiers binaires.
Interface `ncurses` trop choupie.
Devrait peut-être un jour (mais flemme) passer à la couleur.

![Hexdiff](http://tboudet.free.fr/hexdiff/ecran.png "Hexdiff")


## Checkresolv

Vieux truc rudimentaire pour [fouiller](CheckResolv) dans la résolution de nom.
L'adaptation à `IPv6` est à l'étude.
Contrairement à d'autres outils similaires, il utilise les
fonctions de résolution de la `libc` au lieu de s'adresser directement
au serveur de noms.

```
tth@redlady:~/Devel/KlugyTools/CheckResolv$ ./checkresolv -r tetalab.org
------------( tetalab.org 
h_name:       tetalab.org
ip:           89.234.156.223
reverse 89.234.156.223   -> bobby.tetalab.org
```

## Ecoute

Un [joueur](Ecoute) de fichiers musicaux surgi d'un lointain passé,
qui va prendre un coup de jeune pour gérer les formats modernes.


