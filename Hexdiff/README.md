# Hexdiff

Un truc en ncurses pour voir les différences entre deux fichiers binaires.

```
        look at:
                - the Makefile
                - the man page
                - the source code

        old website:
                http://tboudet.free.fr/hexdiff/

        installation:
                1) run make
                2) patch
                3) goto 1

```