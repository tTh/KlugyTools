/*
 *		ifao.c		(libao interface)
 *		------
 */

#include    <stdlib.h>
#include    <unistd.h>
#include    <string.h>
#include    <ao/ao.h>
#include    <sndfile.h>
#include    "ecoute.h"

/*==------------------------------------------------------------------==*/
/*
 *	privates variables of this module. MUST be static !
 */

#define TBUF	100

static int			isactive;
static char			devname[TBUF+1];
static ao_sample_format		AOSF;

/*==------------------------------------------------------------------==*/
/*
 */
int setup_sound_output(int splr, int nbch)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d %d )\n", __func__, splr, nbch);
#endif

if (splr)	AOSF.rate        = splr;
if (nbch)	AOSF.channels    = nbch;

return 0;
}
/*==------------------------------------------------------------------==*/
/*
 *	initialisation of this module who MUST be called
 *	before any use of this module !
 */
int   init_sound_output(char *device, int k)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, device, k);
#endif

/* some default values */
AOSF.rate 		= 44100;
AOSF.channels		= 2;
AOSF.bits		= 16;
AOSF.byte_format	= AO_FMT_LITTLE;		/* XXX ??? */

/*  XXX    see the libao documentation for exact syntax */
if (TBUF < strlen(device)) {
	fprintf(stderr, "buffer overflow in %s\n", __func__);
	exit(1);
	}
strcpy(devname, device);

return 0;
}
/*==------------------------------------------------------------------==*/
int   start_sound_output(int k)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, k);
#endif

return -1;
}
/*==------------------------------------------------------------------==*/
int   stop_sound_output(int k)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, k);
#endif

return -1;
}
/*==------------------------------------------------------------------==*/
int   infos_sound_output(char *title)
{

if (NULL==title) {
	abort();
	}

fprintf(stderr, "   +------ snd out infos (%s)\n", title);
fprintf(stderr, "   |  device name      '%s'\n", devname);
fprintf(stderr, "   |  sample rate      %d\n", AOSF.rate);
fprintf(stderr, "   |  channels         %d\n", AOSF.channels );
fprintf(stderr, "   |  bits/smpl        %d\n", AOSF.bits );
fprintf(stderr, "   |  is active ?      %s\n", isactive ? "yes" : "no");

return -1;
}
/*==------------------------------------------------------------------==*/
void  infos_techniques(void)
{
int		foo;
WINDOW		*popup;
char		*cptr;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( )\n", __func__);
#endif

popup = newwin(12, 60, L_POPUP, C_POPUP);
bordure(popup, 1);
mvwaddstr(popup, 0, 2, "{ Technical infos }");

cptr = sf_version_string();
mvwaddstr(popup, 2, 3, cptr);

/* mvwaddstr(popup, 11, 2, "*   hit any key to reboot  *"); */
wrefresh(popup);
getch();
delwin(popup);
touchwin(stdscr);	refresh();

}
/*==------------------------------------------------------------------==*/

