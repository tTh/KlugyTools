/*
			cleargdbm.c
			===========

	very old homepage = http://krabulator.free.fr/devel/

	=====================================================
	WARNING! this program is _not_ secure! don't use it
	with a setuid root setting.
	=====================================================

			(g) 2003 Thierry Boudet - aka TontonTh
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/stat.h>
#include  <unistd.h>
#include  <string.h>
#include  <ctype.h>
#include  <gdbm.h>

#define  VERSION	"0.0.12" 

#define MAX_FILENAME_LENGTH  4242	// 63 bytes for a filename is enough
					// for all.  -imaginary BG-

char filename[MAX_FILENAME_LENGTH+2];
int	verbosity = 0;
int	block_size = 512;

/*  ---------------------------------------------------------------------  */
static void
print_version(int totale)
{
printf ("\n");
printf ("*** ClearGDBM version %s (g) 2003 TontonTh Useless Soft.\n", VERSION);
printf ("\n");
if (totale)
	{
	printf (" file " __FILE__ " compiled  " __DATE__ " at " __TIME__ "\n");
	printf (" %s\n", gdbm_version);
	printf ("\n");
	}
#if TRACE
printf (" WARNING: compiled with TRACE option. May be _bogus_ !\n\n");
#endif
}
/*  ---------------------------------------------------------------------  */
static void
help(void)
{

}
/*  ---------------------------------------------------------------------  */
static int
clear_gdbm(char *name, int flag)
{
int		foo;
GDBM_FILE	file;
struct stat	infos;
int		mode;

#if TRACE
fprintf(stderr, "TRACE: try to clear gdbm file '%s', flag=%d\n", name, flag);
#endif

/*
 *	on va v�rifier si c'est bien un fichier GDBM qu'on va niquer, l�.
 */
file = gdbm_open(name, 512, GDBM_READER, 0444, NULL);
if (file == NULL)
	{
	fprintf(stderr, "Hu, Ho, '%s' is not a GDBM file, err: %s\n",
					name, gdbm_strerror(gdbm_errno));
	exit(1);
	}
gdbm_close(file);

/*
 *	nous allons r�cuperer quelques infos sur le fichier � n*q**r.
 */
foo = stat(name, &infos);
#if TRACE
fprintf(stderr, "TRACE: retour stat = %d\n", foo);
#endif
mode = infos.st_mode & 0777;
fprintf(stderr, "protection = 0%03o\n", mode);

/*
 *	bon, maintenant, on y va --force
 */
foo = unlink(name);	
if (foo)
	{
	perror ("unlink file");
	exit(1);
	}

file = gdbm_open(name, 512, GDBM_WRCREAT, mode, NULL);
if (file == NULL)
	{
	fprintf(stderr, "Ho, d�sol�, pas pu cr�er %s (%s)\n",
					name, gdbm_strerror(gdbm_errno));
	exit(1);
	}
gdbm_close(file);

return 0;
}
/*  ---------------------------------------------------------------------  */
/*	
 *	let's go for the destroy :)
 */

static char *optstring = "Vvhb:?";

int main(int argc, char *argv[])
{
int	option_retour;
int	foo;
char	*nomfichier;

while ( (option_retour = getopt(argc, argv, optstring)) >= 0)
	{
#if TRACE
	fprintf(stderr, "TRACE: getopt = %d, argc = %d, optind = %d\n",
				 option_retour, argc, optind);
#endif
	switch (option_retour)
		{
		case 'V':
			print_version(1);
			break;

		case 'v':
			verbosity++;
			break;

		case 'h':	case '?':
			help();
			break;

		}
	}

#if TRACE
fprintf(stderr, "TRACE: fin traitement options argc=%d, optind=%d\n",
					argc,	optind);
fprintf(stderr, "TRACE: next arg %s\n", argv[argc-1]);
#endif

nomfichier = argv[argc-1];

/*
 *		WARNING! 
 *
 *	this programme is not secure. carefully read the sources
 *	before use. you have be warned.
 */

clear_gdbm(nomfichier, 0);

return 0;
}

/*  ---------------------------------------------------------------------  */
