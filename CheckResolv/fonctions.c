/*
 *		CheckResolv
 *		===========
 *	quick and dirty debugging hack.
 *
 *	a bunch of various undocumented functions.
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <ctype.h>
#include  <sys/time.h>
#include  <sys/types.h>
#include  <sys/socket.h>
#include  <netinet/in.h>
#include  <netdb.h>
#include  <arpa/inet.h>

#include  "fonctions.h"

/* ------------------------------------------------------------ */
/*
 *	return !0 if str seems to be a quadnumber ip address.
 */
int	is_IP_addr(char *str)
{
int	nbrdots, length, foo, flag;
/* int	o[4]; */

length = strlen(str);
flag = nbrdots = 0;
for (foo=0; foo<length; foo++)
	{
	if (str[foo]=='.')	nbrdots++;
	else if ( ! isdigit(str[foo]) )
		return 0;
	}

return nbrdots==3;
}
/*
 *	warning, this func is a kludge.
 */
/* ------------------------------------------------------------ */
double chronometre(void)
{
struct timeval		tv;
int			foo;
double			ftemps;

foo = gettimeofday(&tv, NULL);
if (foo) perror("get time of day");

ftemps = (double)tv.tv_sec + ((double)tv.tv_usec/1e6);
return ftemps;
}
/* ------------------------------------------------------------ */
int	print_ip(struct in_addr **liste)
{
struct in_addr	*addr;

if (liste==NULL)
	{
	fprintf(stderr, "print addrip (%s:%d): liste is NULL\n",
					__FILE__, __LINE__);
	exit(5);
	}

while ((addr = *liste++)!=NULL)
	{
	printf("ip:           %s", inet_ntoa(*addr));
/*
 *	mmmm, must resolve globals  vars...
	if (verbose)
		printf("   %8x", *addr);
 */
	printf("\n");
	}

return 0;
}
/* ------------------------------------------------------------ */
static void print_onevar(char *name)
{
char	*ptr;

if ((ptr=getenv(name)) == NULL)
	printf("%-29s is not defined\n", name);
else
	printf("%-27s = '%s'\n", name, ptr);
}

struct envvars
	{
	char	*name;
	char	*comment;
	} envvars[] =
{
{	"HOSTNAME",		NULL	},
{	"HOSTALIASES",		NULL	},
{	"http_proxy",		NULL	},
{	"ftp_proxy",		NULL	},
{	"PRINTER",		NULL	},
{	"RESOLV_HOST_CONF",	NULL	},
{	"RESOLV_SERV_ORDER",	NULL	},
{	"RESOLV_SPOOF_CHECK",	NULL	},
{	"RESOLV_MULTI",		NULL	},
{	"RESOLV_REORDER",	NULL	},
{	"CHECKRESOLV",		NULL	},
{	"TONTON_TH",		NULL	},
{	NULL,		NULL	}
};

/*
 *	by the way, if you know another interesting env vars in
 *	the resolver context...
 */

int	print_envvars(int flags)
{
struct envvars	*pev;

puts("");
pev = envvars;
while (pev->name != NULL)
	{
	print_onevar(pev->name);
	pev++;
	}

return 0;
}
/* ------------------------------------------------------------ */
void help_options(void)
{
fputs("checkresolv [-v] [-r] [-e] [-t] hostname|ipaddr\n", stderr);
fputs("checkresolv -h : more help\n", stderr);
fputs("checkresolv -V : print version\n", stderr);
}
/* ------------------------------------------------------------ */
void usage(void)
{
fprintf(stderr, "***  CheckResolv  v %s  ***  by tth  ***\n", VERSION);
fputs("Usage:\n", stderr);
fputs("\tcheckresolv [options] hostname | @ip\n", stderr);
fputs("\n", stderr);
fputs("options:\n", stderr);
fputs("\t-V:      display version number\n", stderr);
fputs("\t-v:      increase verbosity\n", stderr);
fputs("\t-r:      try a reverse lookup\n", stderr);
fputs("\t-e:      print some env vars\n", stderr);
fputs("\t-t:      print elapsed time\n", stderr);
fputs("\n", stderr);
fputs("environmemt variable:\n", stderr);
fputs("\tname:    CHECKRESOLV\n", stderr);
fputs("\tvalue:   \"tech\"  reverse-lookup and timing\n", stderr);
fputs("\tvalue:   \"all\"   all the bells & whistles\n", stderr);
fputs("\n", stderr);
exit(0);
}
/* ------------------------------------------------------------ */
void version(void)
{
printf("CheckResolv v %s by tth\n", VERSION);
}
/* ------------------------------------------------------------ */
