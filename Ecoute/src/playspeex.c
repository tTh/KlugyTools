/*
 *	playspeex.c
 *	-----------			12 Avril 2005
 *
 */

#include   <fcntl.h>
#include   <unistd.h>
#include   <ogg/ogg.h>
#include   <ao/ao.h>		/* for the sound output */

#include   "ecoute.h"

/*==------------------------------------------------------------------==*/
int	speex_player(char *fname, WINDOW *popup)
{
mvwaddstr(popup, 1, 2, fname);
mvwaddstr(popup, 1, 4, "Speex playing: work in progress...");
wrefresh(popup);	sleep(1);

mvwaddstr(popup, 6, 6, "*** COREDUMPING ***");
wrefresh(popup);	getch();

return -1;
}
/*==------------------------------------------------------------------==*/
