/*
 *	playogg.c
 *	---------			2 Mars 2005
 *
 *
 *  2005, Apr 11:	just downloaded some tarball from xiph.org, thinking
 *			about some code samples...
 */

#include   <fcntl.h>
#include   <unistd.h>
#include   <string.h>
#include   <ogg/ogg.h>
#include   <ao/ao.h>		/* for the sound output */

#include   "ecoute.h"

/*==------------------------------------------------------------------==*/
int	ogg_player(char *fname, WINDOW *popup)
{
ogg_sync_state		oy;
char			*oggbuff;
int			ret;

mvwaddstr(popup, 5, 2, fname);
mvwaddstr(popup, 3, 2, "Ogg playing: work in progress...");
wrefresh(popup);	sleep(1);

/* mvwaddstr(popup, 6, 6, "*** COREDUMPING ***"); */
wrefresh(popup);	getch();

return -42; 		/* XXX */

/*
 *	preparation
 */
memset(&oy, 0, sizeof(oy));
ret = ogg_sync_init(&oy);
mvwprintw(popup, 2, 2, "ogg_sync_init -> %d", ret);

oggbuff = ogg_sync_buffer(&oy, T_BUFFER);
mvwprintw(popup, 3, 2, "ogg_sync_buffer -> %p", oggbuff);
wrefresh(popup);
getch();

/*
 *	action !
 */

/*
 *	termination
 */
ogg_sync_clear(&oy);
ogg_sync_destroy(&oy);
getch();

return -1;
}
/*==------------------------------------------------------------------==*/
