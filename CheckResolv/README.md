```
		CheckResolv
		===========

	This is a quick and dirty hack, built for chasing
	a LPRng latency problem. Usage is very simple:
 	you give a hostname or an IP addr, and the soft
	try a few lookup and/or reverse lookup.


	Options are:
		-V	display version
		-r	do a reverse lookup
		-v	increase verbosity
		-e	print relateds env vars
		-t	display time op operations
	

	Have a good life, don't release CO2 in the Ternet...

```
