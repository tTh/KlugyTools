/*
 *	magic.c
 *	=======		26 Avril 2005
 *
 *	reconnaissance heuristique des fichiers sons.
 *	Gruik coding inside.
 */

#include    <stdio.h>
#include    <unistd.h>
#include    <fcntl.h>
#include    <string.h>
#include    "ecoute.h"

/*==------------------------------------------------------------------==*/
#define   T_BUFF_MAGIC	222

int	magic_detect(char *fname, long *pbits)
{
int		fd;
char		buffer[T_BUFF_MAGIC];

if ( (fd=open(fname, O_RDONLY)) < 0 ) {
	perror(fname);
	return -1;
	}
if ( T_BUFF_MAGIC != read(fd, buffer, T_BUFF_MAGIC) ) {
	close(fd);
	return -2;
	}
close(fd);		

/* now, we have the first bytes of the file in the buffer, so we can
   try the divination process... */

*pbits = 0L;

if (0 == strncmp(buffer, "RIFF", 4))
	{
	if (0 == strncmp(buffer+8, "WAVE", 4))
		{
		return SON_WAV;
		}
	}

if (0 == strncmp(buffer, ".snd", 4))
	{
	return SON_AU;
	}

if (0 == strncmp(buffer, "OggS", 4))
	{
	if (0 == strncmp(buffer+28, "\x01vorbis", 7))
		{
		return SON_OGG;
		}
	if (0 == strncmp(buffer+28, "Speex   ", 7))
		{
		return SON_SPEEX;
		}
	}

/* please add FLAC magic here XXX */


/* really don't know, baylout */
return SON_UNKNOW;
}
/*==------------------------------------------------------------------==*/
