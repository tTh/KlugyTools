/*
 *	fonctions.c		Ecoute: a sound player.
 *	-----------		23 Fev 2005 - dans le froid de Frontignan
 */

#include    <stdio.h>
#include    <string.h>
#include    <sys/types.h>
#include    <sys/stat.h>

#include    "ecoute.h"

/*==------------------------------------------------------------------==*/
long	taille_fichier(int fd)
{
struct stat	st;
int		foo;

foo = fstat(fd, &st);
if (foo) {
	perror("fonctions.c:18 fstat");
	return -1;
	}
return st.st_size;
}
/*==------------------------------------------------------------------==*/
struct un_type_de_fichier {
	char	*extension;
	int	numtype;
	char	*descr;
	} types_de_fichiers[] = {
 {	".note",	SON_NOTE,	"tTh .note files"		},
 {	".wav",		SON_WAV,	"Microsoft Wave"		},
 {	".ogg",		SON_OGG,	"Xiph Ogg/Vorbis"		},
 {	".speex",	SON_SPEEX,	"Xiph Compressed speach"	},
 {	".au",		SON_AU,		"Sun/NeXT audio data"		},
 {	".flac",	SON_FLAC,	"Free Lossless Audio Codec"	},
 {	".opus",	SON_OPUS,	"OPUS"				},
 {	".mp3",		SON_MP3,	"mp3"				},
};
#define NBR_TYPES 	\
	(sizeof(types_de_fichiers)/sizeof(struct un_type_de_fichier))

/*==------------------------------------------------------------------==*/
/*
 *	this function is based on the .extension of the filename.
 *	this is a very crude system, because we run on Unix, where
 *	file extensions are not a hard law.
 *
 *	see also 'magic.c' for a more advanced function...
 */
int	type_du_fichier(char *nom)
{
int		foo, len_nom, len_ext;

#if DEBUG_LEVEL > 1
fprintf(stderr, "type_du_fichier(%s)\n", nom);
#endif
len_nom = strlen(nom);
for (foo=0; foo<(int)NBR_TYPES; foo++) {
	len_ext = strlen(types_de_fichiers[foo].extension);
	if ( (len_nom > len_ext) &&
	     !strcmp(nom+(len_nom-len_ext), types_de_fichiers[foo].extension) ) {
#if DEBUG_LEVEL > 1
		fprintf(stderr, "   found %s\n", types_de_fichiers[foo].descr);
#endif
		return(types_de_fichiers[foo].numtype);
		}
	}

#if DEBUG_LEVEL > 1
fprintf(stderr, "    fail...\n");
#endif
return -1;
}
/*==------------------------------------------------------------------==*/
char	*type2ext(int type)
{
int	foo;

if (type == SON_UNKNOW)		return "unknow";

for (foo=0; foo<(int)NBR_TYPES; foo++) {
	if (type==types_de_fichiers[foo].numtype)
		return (types_de_fichiers[foo].extension)+1;
	}
return "???";
}
/*==------------------------------------------------------------------==*/
char	*type2descr(int type)
{
int	foo;

if (type == SON_UNKNOW)		return "unknow";

for (foo=0; foo<(int)NBR_TYPES; foo++) {
	if (type==types_de_fichiers[foo].numtype)
		return (types_de_fichiers[foo].descr);
	}
return "lp0 on fire";
}
/*==------------------------------------------------------------------==*/
int	play_this_file(char *nom, int type, int flags)
{
int		foo;
WINDOW		*popup;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d 0x%x )\n", __func__, nom, type, flags);
#endif

if (NULL==nom) {
	/* molly-guard */
	fprintf(stderr, "%s:%s called with a NULL name\n", __FILE__, __func__);
	return -1;
	}
/*
 *	ncurses initial stuff
 */
popup = newwin(12, 60, L_POPUP, C_POPUP);
bordure(popup, 1);
/* wstandout(popup); */
mvwaddstr(popup, 0, 2, "{ playing ");
mvwaddstr(popup, 0, 12, nom);
mvwaddstr(popup, 0, 12+strlen(nom), " }");
/*wstandend(popup); */
wrefresh(popup);

foo = start_sound_output(0);

switch (type)
	{
	case SON_NOTE:
		foo = note_player(nom, popup);
		break;	
	case SON_WAV:
		foo = wav_player(nom, popup);
		break;
	case SON_OGG:
		foo = ogg_player(nom, popup);
		break;
	case SON_AU:
		foo = au_player(nom, popup);
		break;
	case SON_SPEEX:
		foo = speex_player(nom, popup);
		break;
	case SON_FLAC:
		foo = flac_player(nom, popup);
		break;
	}

(void)stop_sound_output(0);

/*
 *	screen washing.
 */
delwin(popup);
touchwin(stdscr);	refresh();

#if DEBUG_LEVEL
fprintf(stderr, "    end of %s\n", __func__);
#endif

return foo;
}
/*==------------------------------------------------------------------==*/
/*
 *	mey be, here, we can use the internals functions
 *	of libsndfile ?
 */
int	info_about_this_file(char *nom, int type)
{
int		foo;
WINDOW		*popup;
char		chaine[100];
struct stat	st;

/*
 *	ncurses initial stuff
 */
popup = newwin(12, 64, L_POPUP, C_POPUP);
bordure(popup, 1);
wstandout(popup);
mvwaddstr(popup, 0, 2, "{ infos about ");
mvwaddstr(popup, 0, 16, nom);
mvwaddstr(popup, 0, 16+strlen(nom), " }");
wstandend(popup);
wrefresh(popup);

/*
 *	filesystem infos
 */
foo = stat(nom, &st);
if (foo != 0) {
	mvwaddstr(popup, 2, 2, "hu ho, stat failed ?");
	}
else	{
	sprintf(chaine, "owned by:  user id %d, group id %d", 
			st.st_uid, st.st_gid);
	mvwaddstr(popup, 2, 2, chaine);
	sprintf(chaine, "size:      %ld bytes or %ld MB.",
				st.st_size,
				1 + (st.st_size / (1024*1024)) );
	mvwaddstr(popup, 3, 2, chaine);
	}

/*
 *	internals infos
 */
sprintf(chaine, "magic_detect: %4d  /  ", type);
mvwaddstr(popup, 6, 2, chaine);
mvwaddstr(popup, 6, 25, type2descr(type));
wrefresh(popup);
getch();

/*
 *	screen cleanup.
 */
delwin(popup);
touchwin(stdscr);	refresh();

return foo;
}
/*==------------------------------------------------------------------==*/
