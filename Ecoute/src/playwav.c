/*
 *	playwav.c
 *	---------			23 Fevrier 2005
 */

#include   <fcntl.h>
#include   <string.h>
#include   <sndfile.h>
#include   <ao/ao.h>		/* for the sound output */

#include   "ecoute.h"

/*==------------------------------------------------------------------==*/
int	wav_player(char *fname, WINDOW *popup)
{
SNDFILE			* sndf;
SF_INFO			sfinfo;
int			driver;
ao_sample_format	format;
ao_device		* device;
char			chaine[120];
short			* samples;
int			lu, key;
long			total_lu;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, fname, popup);
#endif

/*
 *	get help from 'sndfile' for getting datas
 */
memset(&sfinfo, 0, sizeof(sfinfo));
sndf = sf_open(fname, SFM_READ, &sfinfo);
if (NULL==sndf)
	{
	mvwaddstr(popup, 4, 4, "Invalid file ?");
	wrefresh(popup);
	getch();
	delwin(popup);
	touchwin(stdscr);	refresh();
	return -1;
	}

sprintf(chaine, "frames      %lu", (unsigned long)sfinfo.frames);
mvwaddstr(popup, 2, 5, chaine);
sprintf(chaine, "smplrate    %d", sfinfo.samplerate);
mvwaddstr(popup, 3, 5, chaine);
sprintf(chaine, "channels    %d", sfinfo.channels);
mvwaddstr(popup, 4, 5, chaine);
sprintf(chaine, "format      0x%x", sfinfo.format);
mvwaddstr(popup, 5, 5, chaine);
sprintf(chaine, "sections    %d", sfinfo.sections);
mvwaddstr(popup, 6, 5, chaine);
wrefresh(popup);

#if DEBUG_LEVEL > 1
getch();					/* XXX */
#endif

/*
 *	memory allocation
 *	warning, we can get multiples channels.
 */
if ( NULL == (samples = malloc(T_BUFFER*sizeof(short))) )
	{
	perror("memory shortage in WAV player");
	abort();
	}

/*
 *	initialize the "Cabasse Sampan" output device (aka libao)
 */
ao_initialize();
driver = ao_default_driver_id();
/* XXX valeurs provisoires XXX */
memset(&format, 0, sizeof(ao_sample_format));
format.bits		= 16;
format.channels		= sfinfo.channels;
format.rate		= sfinfo.samplerate;
format.byte_format	= AO_FMT_LITTLE;		/* XXX ??? */
device = ao_open_live(driver, &format, NULL);
if (NULL==device)
	{
	fprintf(stderr, "\nEcoute: Error open device\n");
	/*
	 *  comment connaitre la cause de l'erreur ?
	 */
	abort();  
	}

/*
 *	**********************
 *	*  the playing loop  *
 *	**********************
 */
total_lu = 0L;
while ( (lu=sf_read_short(sndf, samples, T_BUFFER)) > 0 )
	{
	ao_play(device, (char *)samples, lu*2);
	total_lu += (long)lu;
	progress_bar(popup, total_lu/sfinfo.channels, sfinfo.frames, 0);
	wrefresh(popup);

	/* HERE WE HAVE TO DETECT A 'STOP LISTEN' FUNCTION. */
	if (is_a_key_hitted()) {
		key = getch();
		fprintf(stderr, "%s key pressed 0x%X\n", __func__, key);
		if ('X' == key) {
			break;
			}
		}
	}

/*
 *	shutdown "La voix du Theatre"
 */
ao_close(device);	ao_shutdown();	

/*
 *	memory release
 */
free(samples);

/*
 *	releasing 'sndfile' ressources
 */
sf_close(sndf);

#if DEBUG_LEVEL
fprintf(stderr, "    end of %s\n", __func__);
#endif

return -1;
}
/*==------------------------------------------------------------------==*/
