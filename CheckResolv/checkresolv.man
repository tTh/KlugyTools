.TH CheckResolv 1 "2005 September" "various OS" "Tonton Th"

.SH NAME
checkresolv \- quick & dirty tool for checking sanity of resolver.

.SH SYNOPSIS
\fBcheckresolv\fP -V
.br
\fBcheckresolv\fP [options] host.name
.br
\fBcheckresolv\fP [options] 42.51.69.406

.SH DESCRIPTION
CheckResolv is a (not so) q'n'd tool for checking basic capacities and
potentials bugs of your resolver configuration.
I have started the dev of this thing when I was working hard on a remote
nasty printing problems.
It use only \fBgethostbyname\fP and \fBgethostbyaddr\fP functions,
like any lambda applications.
CheckResolv can also display environment variables who can modify the behaviour
of this damned resolver, or give you some problems.

.SH OPTIONS
.B -V
display version number and exit.
.br
.B -h
brief help on the command-line options.
.br
.B -v
display various useless messages.
.br
.B -r
do reverse lookup on IP address discovered.
.br
.B -e
show content of a few environment vars resolver-related.
.br
.B -t
compute elapsed time of operation, not really accurate.

.SH TIPS
If you have a strange problem between two hosts, "don't panic". Take the time
to compil CheckResolv on the two hosts, and run it with the -r option, first
with the hostname, and after that, with the IP address.
And do this two runs on the two hosts. So, you get four results.
Now, use your brain...

.SH CONFIG FILE
All your config files are belong to us. In the futur, may be you can
put a list of env vars to display in a file.

.SH ENV VARS
If the environment variable CHECKRESOLV is set to "tech", reverse lookup
and timing are activated, very nice when your pop3 server take 42 seconds
before you can get some silly answer.
And "all" give you all the bells & whistles.
This variable override command lines options.

.SH SEE ALSO
.BR resolver (5),
.BR host (1),
.BR dig (1),
.BR nslookup (1)

.SH BUGS
Some are here, some are waiting in the dark. Hidden static storage is a pain.
No IPv6 support. No Slackware package, no Debian package...

.SH COMPILATION
Currently (2005 march), this software was only tested on Slackware 10.0 and
Debian Sarge, but it can
compile 'fine' on others Linux or *BSD flavors. IRIX is also a valuable target,
because my troll-master use it...

.SH AUTHOR(S)
Thierry (aka tth) Boudet, with help from @donis, Miod, Reynerie...



