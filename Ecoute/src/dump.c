/*
 *	dump.c
 *	======	debug module for 'Ecoute' - 2005.03.06
 *
 *  Warning: gruik coding inside.
 */

#include    <unistd.h>
#include    <fcntl.h>
#include    <string.h>
#include    <ctype.h>
#include    "ecoute.h"

/*==------------------------------------------------------------------==*/
#define T_BUFF_HEX	(16*16)

static int hexadump(char *fname, WINDOW *w)
{
int		fd;
unsigned char	buffer[T_BUFF_HEX];
int		lig, col, idx;
char		chaine[10], lettre;

#if DEBUG_LEVEL
fprintf(stderr, "hexadump of %s\n", fname);
#endif

if ( (fd=open(fname, O_RDONLY)) < 0 ) {
	mvwaddstr(w, 2, 2, "err open");		wrefresh(w);
	getch();
	return -1;
	}

if ( T_BUFF_HEX != read(fd, buffer, T_BUFF_HEX) ) {
	mvwaddstr(w, 2, 2, "err read");		wrefresh(w);
	getch();
	}
close(fd);

for (lig=0; lig<16; lig++) {
	for (col=0; col<16; col++) {
		idx = (lig*16) + col;
		sprintf(chaine, "%02x", buffer[idx]);
		mvwaddstr(w, lig+1, (col*3)+1, chaine);
		if (isprint(buffer[idx]))
			lettre = buffer[idx];
		else
			lettre = '.';
		mvwaddch(w, lig+1, col+50, lettre);
		}
	}
wrefresh(w);

return 0;
}
/*==------------------------------------------------------------------==*/
/*
 *	public entry point.
 */
int dump_this_file(char *fname, int flag)
{
WINDOW		*popup;

#if DEBUG_LEVEL
fprintf(stderr, ">> dump_this_file ( '%s' %d )\n", fname, flag);
#endif

popup = newwin(18, 68, L_POPUP, C_POPUP);
bordure(popup, 1);
mvwaddstr(popup, 0, 2, "{ dump of ");
mvwaddstr(popup, 0, 12, fname);
mvwaddstr(popup, 0, 12+strlen(fname), " }");
wrefresh(popup);

(void)hexadump(fname, popup);

wrefresh(popup);
getch();
delwin(popup);	
touchwin(stdscr); refresh();
return -1;
}
/*==------------------------------------------------------------------==*/
